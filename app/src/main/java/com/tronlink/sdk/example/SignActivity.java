package com.tronlink.sdk.example;

import static com.tronlink.sdk.TronLinkSdk.INTENT_LOGIN_REQUESTCODE;
import static com.tronlink.sdk.TronLinkSdk.INTENT_LOGIN_RESULT;
import static com.tronlink.sdk.TronLinkSdk.INTENT_PAY_IS_NOT_TRANSACTION_RESULT;
import static com.tronlink.sdk.TronLinkSdk.INTENT_PAY_JSON_REQUESTCODE;
import static com.tronlink.sdk.TronLinkSdk.INTENT_PAY_JSON_RETURN_JSON_REQUESTCODE;
import static com.tronlink.sdk.TronLinkSdk.INTENT_PAY_REQUESTCODE;
import static com.tronlink.sdk.TronLinkSdk.INTENT_PAY_RESULT;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tronlink.sdk.TronLinkSdk;
import com.tronlink.sdk.bean.Account;
import com.tronlink.sdk.bean.Param;
import com.tronlink.sdk.bean.ResourceMessage;
import com.tronlink.sdk.bean.Walllet;
import com.tronlink.sdk.sdkinterface.ITronLinkSdk;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

public class SignActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView mValueTv;
    private Walllet mWallet;
    private static final String TAG = "MainActivity";
    private String mToAddress = "TX2rdGSexVCy6mAgDk3Vgk8La1FuheXTbF";
    private ITronLinkSdk mTronSdk;
    private EditText mToAddressEt,mFromAddressEt ;
    private EditText mPayJsonEt, mSignStrReturnEt;
    private static final String NO_ADDRESS = "Not getting current wallet address";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
        mTronSdk = TronLinkSdk.getInstance();
        initView();
        initListener();
    }
    private void initView() {
        mValueTv = findViewById(R.id.et_signed_hash);
        mToAddressEt = findViewById(R.id.et_to_address);
        mToAddressEt.setText("TX2rdGSexVCy6mAgDk3Vgk8La1FuheXTbF");
        mFromAddressEt = findViewById(R.id.et_from_address);
        mPayJsonEt = findViewById(R.id.et_pay_json);
        String str = "{\"txID\":\"8cf4816c98706a1d7cde7bee3e74d7b57f3a0cc35d7bbc791022ac389e1ed6ef\",\"raw_data\":{\"contract\":[{\"parameter\":{\"value\":{\"data\":\"0a7bc885000000000000000000000000d51f8e9d2ef0a7317b758541b0a552706bedb3e80000000000000000000000000000000000000000000000000000000ccf91178000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010642a940000000000000000000000000000000000000000000000000000000000001386\"," +
                "\"owner_address\":" +
                "\"" + mFromAddressEt.getText() + "\"," +
                "\"contract_address\":" +
                "\"" + mToAddressEt.getText() + "\"," +
                "\"call_value\":100},\"type_url\":\"type.googleapis.com/protocol.TriggerSmartContract\"},\"type\":\"TriggerSmartContract\"}],\"ref_block_bytes\":\"bb46\",\"ref_block_hash\":\"f6e2284f3ae54ec5\",\"expiration\":1556246946000,\"fee_limit\":1000000000,\"timestamp\":1556246887501},\"raw_data_hex\":\"0a02bb462208f6e2284f3ae54ec540d0c1afbca52d5a9602081f1291020a31747970652e676f6f676c65617069732e636f6d2f70726f746f636f6c2e54726967676572536d617274436f6e747261637412db010a1541a58269f525bf0bf7a80df7979a4775c6172e796c121541b3bddae866b2ce2349bdcb59dfbfa1a75f8552da1894d590830122a4010a7bc885000000000000000000000000d51f8e9d2ef0a7317b758541b0a552706bedb3e80000000000000000000000000000000000000000000000000000000ccf91178000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010642a94000000000000000000000000000000000000000000000000000000000000138670cdf8abbca52d90018094ebdc03\"}";
        mPayJsonEt.setText(str);
        mSignStrReturnEt = findViewById(R.id.et_sign_str_return);

    }

    private void updateTransactionData(){
        String str = "{\"txID\":\"8cf4816c98706a1d7cde7bee3e74d7b57f3a0cc35d7bbc791022ac389e1ed6ef\",\"raw_data\":{\"contract\":[{\"parameter\":{\"value\":{\"data\":\"0a7bc885000000000000000000000000d51f8e9d2ef0a7317b758541b0a552706bedb3e80000000000000000000000000000000000000000000000000000000ccf91178000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010642a940000000000000000000000000000000000000000000000000000000000001386\"," +
                "\"owner_address\":" +
                "\"" + mFromAddressEt.getText() + "\"," +
                "\"contract_address\":" +
                "\"" + mToAddressEt.getText() + "\"," +
                "\"call_value\":100},\"type_url\":\"type.googleapis.com/protocol.TriggerSmartContract\"},\"type\":\"TriggerSmartContract\"}],\"ref_block_bytes\":\"bb46\",\"ref_block_hash\":\"f6e2284f3ae54ec5\",\"expiration\":1556246946000,\"fee_limit\":1000000000,\"timestamp\":1556246887501},\"raw_data_hex\":\"0a02bb462208f6e2284f3ae54ec540d0c1afbca52d5a9602081f1291020a31747970652e676f6f676c65617069732e636f6d2f70726f746f636f6c2e54726967676572536d617274436f6e747261637412db010a1541a58269f525bf0bf7a80df7979a4775c6172e796c121541b3bddae866b2ce2349bdcb59dfbfa1a75f8552da1894d590830122a4010a7bc885000000000000000000000000d51f8e9d2ef0a7317b758541b0a552706bedb3e80000000000000000000000000000000000000000000000000000000ccf91178000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010642a94000000000000000000000000000000000000000000000000000000000000138670cdf8abbca52d90018094ebdc03\"}";
        mPayJsonEt.setText(str);
    }

    private void getValue() {
        mToAddress = mToAddressEt.getText().toString();
    }

    private void initListener() {
        findViewById(R.id.bt_login).setOnClickListener(this);
        findViewById(R.id.bt_sign_str_return).setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_login:
                login();
                break;
            case R.id.bt_sign_str_return:
                toSignStrReturn();
                break;
        }

    }

    /**
     * login will return account address
     */
    private void login() {
        mTronSdk.authLogin(SignActivity.this);
    }




    /**
     * create transaction json trx
     */
    private String createTransactionJson(int type) {
        getValue();
        if (mWallet == null || TextUtils.isEmpty(mWallet.getAddress())) {
            Toast.makeText(SignActivity.this, NO_ADDRESS, Toast.LENGTH_LONG).show();
            return null;
        } else {
            return null;
        }
    }


    private void toSignStrReturn() {
        if (mWallet == null || TextUtils.isEmpty(mWallet.getAddress())) {
            Toast.makeText(SignActivity.this, NO_ADDRESS, Toast.LENGTH_LONG).show();
        } else {
            String unSignStr = mSignStrReturnEt.getText().toString();
            if (!TextUtils.isEmpty(unSignStr)) {
                mTronSdk.toPayReturnSign(SignActivity.this, unSignStr, mWallet.getName());
            }
            else {
                Toast.makeText(SignActivity.this, "unsign string is empty", Toast.LENGTH_LONG).show();
            }
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == INTENT_LOGIN_REQUESTCODE) {
            if (resultCode == RESULT_OK) {
                if (data != null && data.getStringExtra(INTENT_LOGIN_RESULT) != null) {
                    mWallet = new Gson().fromJson(data.getStringExtra(INTENT_LOGIN_RESULT), Walllet.class);
                    if (mWallet != null){
                        mFromAddressEt.setText(mWallet.getAddress());
                        updateTransactionData();
                    }

                        //Toast.makeText(this, "login success, address:" + mWallet.getAddress(), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(this, "login cancel", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == INTENT_PAY_REQUESTCODE || requestCode == INTENT_PAY_JSON_REQUESTCODE) {
            boolean isSucc = false;
            if (data != null && data.getExtras() != null)
                isSucc = data.getBooleanExtra(INTENT_PAY_RESULT, false);
            Toast.makeText(this, "pay is " + (isSucc ? "success" : "fail"), Toast.LENGTH_LONG).show();
        }  else if (requestCode == INTENT_PAY_JSON_RETURN_JSON_REQUESTCODE) {
            String jsonStr = null;
            boolean isNotTransaction = false;
            if (data != null && data.getExtras() != null) {
                jsonStr = data.getStringExtra(INTENT_PAY_RESULT);
                isNotTransaction = data.getBooleanExtra(INTENT_PAY_IS_NOT_TRANSACTION_RESULT, false);
            }
            mValueTv.setText(jsonStr);
//            if(isNotTransaction){
//                Toast.makeText(this, "return signed str:" + jsonStr, Toast.LENGTH_LONG).show();
//            }
//            else {
//                Toast.makeText(this, "return json:" + jsonStr, Toast.LENGTH_LONG).show();
//            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
